// RFM69 MQTT gateway sketch
//
// This gateway relays messages between a MQTT-broker and several wireless nodes and will:
// - receive sensor data from several nodes periodically and on-demand
// - send/receive commands from the broker to control actuators and node parameters
//
//	Connection to the MQTT broker is over a fixed ethernet connection:
//
//		The MQTT topic is /home/rfm_gw/direction/nodeid/devid
//		where direction is: southbound (sb) towards the remote node and northbound (nb) towards MQTT broker
//
//	Connection to the nodes is over a closed radio network:
//
//		RFM Message format is: nodeID/deviceID/command/integer/float/string
//		where Command = 1 for a read request and 0 for a write request
//
//	Current defined gateway devices are:
//	0	uptime:			gateway uptime in minutes 
//	3	Version:		read version gateway software
//	
//	Reserved ranges for node devices, as implemented in the gateway are:
//	0  - 16				Node system devices
//	16 - 32				Binary output (LED, relay)
//	32 - 40				Integer output (pwm, dimmer)
//	40 - 48				Binary input (button, switch, PIR-sensor)
//	48 - 64				Real input (temperature, humidity)
//	64 - 72				Integer input (light intensity)
//
//	72	string:			transparant string transport
//
//	73 - 90		Special devices not implemented in gateway (yet)
//
//	Currently defined error messages are:
//	90	error:			Tx only: error message if no wireless connection
//	91	error:			Tx only: syntax error
//	92	error:			Tx only: invalid device type
//	99	wakeup:			Tx only: sends a message on node startup
//
//	==> Note: 
//		- Interrupts are disabled during ethernet transactions in w5100.h (ethernet library)
//		  (See http://harizanov.com/2012/04/rfm12b-and-arduino-ethernet-with-wiznet5100-chip/)
//		- Ethernet card and RFM68 board default use the same Slave Select pin (10) on the SPI bus;
//		  To avoid conflict the RFM module is controlled by another SS pin (8).
//
//
// RFM69 Library by Felix Rusu - felix@lowpowerlab.com
// Get the RFM69 library at: https://github.com/LowPowerLab
// Get the MQQT client library at: https://github.com/knolleary/pubsubclient
//
// version 1.8 - by Computourist@gmail.com december 2014
// version 1.9 - fixed resubscription after network outage  Jan 2015
// version 2.0 - increased payload size; standard device types; trim float values; uptime & version function gateway;	Jan 2015
// version 2.1 - implemented string device 72; devices 40-48 handled uniformly		Feb 2015
// version 2.2 - changed handling of binary inputs to accomodate Openhab: message for ON and OFF on statechange; 
//			   - RSSI value changed to reception strength in the gateway giving a more accurate and uptodate value ; March 2015
// version 2.3 - System device 9 (number of retransmissions) implemented in gateway	; 
//			   - Deleted debug option 's' to toggle push interval due to memory constraints;  Oct 2015
// version 2.4 - fixed function declaration to comply with new Arduino IDE;
//			   - changed debug routines to comply with memory constraints; 		Jan 2016


#include <RFM69.h>
#include <SPI.h>


#define DEBUGRADIO					// uncomment for radio debugging
#define DEBUG       // uncomment for MQTT debugging
#define VERSION "GW V2.4"


// Wireless settings
#define NODEID 1				// unique node ID in the closed radio network; gateway is 1
#define RFM_SS 10				// Slave Select RFM69 is connected to pin 8
#define NETWORKID 100				// closed radio network ID

//Match frequency to the hardware version of the radio (uncomment one):
#define FREQUENCY RF69_433MHZ
//#define FREQUENCY RF69_868MHZ
//#define FREQUENCY RF69_915MHZ

#define ENCRYPTKEY "GoldenEyesSensor"       // 16-char encryption key; same as on Gateway!
#define IS_RFM69HW 				// uncomment only for RFM69HW! Leave out if you have RFM69W!
#define ACK_TIME 50 				// max # of ms to wait for an ack

// PIN settings
#define MQCON 7					// MQTT Connection indicator
#define R_LED 7					// Radio activity indicator
#define SERIAL_BAUD 115200
#define GREENLED 8

typedef struct {				// Radio packet structure max 66 bytes
int		nodeID;				// node identifier
int		devID;				// device identifier 0 is node; 31 is temperature, 32 is humidity
int		cmd;				// read or write
long		intVal;				// integer payload
float		fltVal;				// floating payload
char		payLoad[32];			// char array payload
} Message;

Message mes;

int	dest;				// destination node for radio packet
int     DID;                    		// Device ID
int 	error;					// Syntax error code
long	lastMinute = -1;			// timestamp last minute
long	upTime = 0;				// uptime in minutes
bool	Rstat = false;				// radio indicator flag
bool	mqttCon = false;			// MQTT broker connection flag
bool	respNeeded = false;			// MQTT message flag in case of radio connection failure
bool	mqttToSend = false;			// message request issued by MQTT request
bool	promiscuousMode = false;		// only receive closed network nodes
bool	verbose = true;				// generate error messages
bool	IntMess, RealMess, StatMess, StrMess;	// types of messages
long	onMillis;				// timestamp when radio LED was turned on
//char	*subTopic = "home/rfm_gw/sb/#";		// MQTT subscription topic ; direction is southbound
//char	*clientName = "RFM_gateway";		// MQTT system name of gateway
char	buff_topic[30];				// MQTT publish topic string
char	buff_mess[32];				// MQTT publish message string

RFM69 radio;

//
//==============	SETUP
//

void setup() {
#ifdef DEBUG
	Serial.begin(SERIAL_BAUD);
#endif
#ifdef DEBUGRADIO
  Serial.begin(SERIAL_BAUD);
#endif
//radio.setCS(RFM_SS);					// change default Slave Select pin for RFM
radio.initialize(FREQUENCY,NODEID,NETWORKID);		// initialise radio module
#ifdef IS_RFM69HW
	radio.setHighPower(); 					// only for RFM69HW!
#endif
radio.encrypt(ENCRYPTKEY);				// encrypt with shared key
radio.promiscuous(promiscuousMode);			// listen only to nodes in closed network

pinMode(GREENLED, OUTPUT);              // set LED output
pinMode(R_LED, OUTPUT);					// set pin of radio indicator
pinMode(MQCON, OUTPUT);					// set pin for MQTT connection indicator
digitalWrite(MQCON, LOW);  				// switch off MQTT connection indicator
digitalWrite(R_LED, LOW);				// switch off radio indicator
digitalWrite(GREENLED, HIGH);
Serial.println("Ready");
}	// end setup

//
//==============	MAIN
//

void loop() {

if (radio.receiveDone()) { processPacket();}	// check for received radio packets and construct MQTT message

}	// end loop

//
//==============	PROCESSPACKET
//
// receives data from the wireless network, parses the contents and constructs MQTT topic and value

void processPacket() {
Rstat = true;							// set radio indicator flag 
//digitalWrite(R_LED, HIGH);					// turn on radio LED
onMillis = millis();						// store timestamp

if (radio.DATALEN != sizeof(mes))				// wrong message size means trouble
#ifdef DEBUGRADIO
	Serial.println("inv msg strct")
#endif
;
else								// message size OK...
{
	mes = *(Message*)radio.DATA;				// copy radio packet
								// and construct MQTT northbound topic
		
}
DID = mes.devID;						// construct MQTT message, according to device ID

StrMess = true;			// String in payload

if (StrMess) {
int i; for (i=0; i<32; i++){ 
	buff_mess[i] = (mes.payLoad[i]); 
}
}	
//use sting, combine all and print
#ifdef DEBUG
	String pair;
	
	pair = DID;
	pair +=",";
	pair += buff_mess;
	Serial.println(pair);
#endif


if (radio.ACKRequested()) {
	radio.sendACK();
	digitalWrite(R_LED, HIGH);
  	delay(10);
  	digitalWrite(R_LED, LOW);
	}			// reply to any radi`o ACK requests

}	// end processPacket
