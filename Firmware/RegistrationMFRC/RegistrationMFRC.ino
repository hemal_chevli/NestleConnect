/*
Arduino 1.6.6
Date:11-4-2016

registration device
TODO preetify serial output
*/

#include <MFRC522.h>
#include <SPI.h>
#include "pitches.h"

#define RFID_RST  5       // reset pin RC522
#define RFID_SS   6        // Slave Select p
#define REDLED   7 
#define GREENLED 8
#define BUZZER   9

int melody[] = {NOTE_B5,NOTE_G4,NOTE_F3,NOTE_B5,NOTE_G4};
int noteDurations[] = {4,8,8,4,8};
int sadNoteDurations[] = {4,4,4,4,4};
 
MFRC522 rfid(RFID_SS, RFID_RST); // Instance of the class

MFRC522::MIFARE_Key key; 

// Init array that will store new NUID 
byte nuidPICC[3];

void setup(){ 
    for (int thisNote = 0; thisNote < 5; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(9);
  }
    pinMode(REDLED, OUTPUT);              // set LED output
    pinMode(GREENLED, OUTPUT);              // set LED output
    pinMode(BUZZER, OUTPUT);              // set LED output
    digitalWrite(REDLED, LOW);
    digitalWrite(GREENLED, LOW);
    digitalWrite(BUZZER, LOW);

    Serial.begin(115200);
    SPI.begin(); // Init SPI bus
      rfid.PCD_Init(); // Init MFRC522 

     for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    digitalWrite(REDLED, HIGH);
    digitalWrite(GREENLED, HIGH);
    delay(500);
    digitalWrite(REDLED, LOW);
}

void loop(){
   
  // Look for new cards
  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;

  //Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  //Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    //Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }

  if (rfid.uid.uidByte[0] != nuidPICC[0] || 
    rfid.uid.uidByte[1] != nuidPICC[1] || 
    rfid.uid.uidByte[2] != nuidPICC[2] || 
    rfid.uid.uidByte[3] != nuidPICC[3] ) {
    //Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
   
    //Serial.println(F("The NUID tag is:"));
    //Serial.print(F("In hex: "));
    //printHex(rfid.uid.uidByte, rfid.uid.size);
    //Serial.println();
    //Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
    digitalWrite(GREENLED, HIGH);
    digitalWrite(BUZZER, HIGH);
    delay(500);
    digitalWrite(GREENLED, LOW);
    digitalWrite(BUZZER, LOW);
  }
  else {
    digitalWrite(REDLED, HIGH);
    digitalWrite(BUZZER, HIGH);
    delay(100);
    digitalWrite(BUZZER, LOW);
    delay(100);
    digitalWrite(BUZZER, HIGH);
    delay(100);
    digitalWrite(BUZZER, LOW);
    delay(100);
    digitalWrite(BUZZER, HIGH);
    delay(100);
    digitalWrite(BUZZER, LOW);
    delay(100);
    digitalWrite(REDLED, LOW);
  }//Serial.println(F("Card read previously."));

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}


/**
 * Helper routine to dump a byte array as hex values to Serial. 
 */
void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? "" : "");
    Serial.print(buffer[i], DEC);
  }
}