/*
Arduino 1.6.6
Date:11-4-2016

registration device
TODO preetify serial output
*/


#include <SPI.h>
#include <RFID.h>
#include "pitches.h"

#define RST_PIN  5
#define SS_PIN   6

#define REDLED   7 
#define GREENLED 8
#define BUZZER   9

int melody[] = {NOTE_B5,NOTE_G4,NOTE_F3,NOTE_B5,NOTE_G4};
int noteDurations[] = {4,8,8,4,8};
int sadNoteDurations[] = {4,4,4,4,4};

RFID rfid(SS_PIN, RST_PIN); 

// Setup variables:
    int serNum0;
    int serNum1;
    int serNum2;
    int serNum3;
    int serNum4;

void setup(){ 
    for (int thisNote = 0; thisNote < 5; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(9);
  }
    pinMode(REDLED, OUTPUT);              // set LED output
    pinMode(GREENLED, OUTPUT);              // set LED output
    pinMode(BUZZER, OUTPUT);              // set LED output
    digitalWrite(REDLED, LOW);
    digitalWrite(GREENLED, LOW);
    digitalWrite(BUZZER, LOW);

    Serial.begin(115200);
    SPI.begin(); 
    rfid.init();
    digitalWrite(REDLED, HIGH);
    digitalWrite(GREENLED, HIGH);
    delay(500);
    digitalWrite(GREENLED, LOW);
    
}

void loop(){
    
    if (rfid.isCard()) {
        if (rfid.readCardSerial()) {
            if (rfid.serNum[0] != serNum0
                || rfid.serNum[1] != serNum1
                || rfid.serNum[2] != serNum2
                || rfid.serNum[3] != serNum3
               // || rfid.serNum[4] != serNum4
            ) {
                /* With a new cardnumber, show it. */
                //Serial.println(" ");
                //Serial.println("Card found");
                serNum0 = rfid.serNum[0];
                serNum1 = rfid.serNum[1];
                serNum2 = rfid.serNum[2];
                serNum3 = rfid.serNum[3];
                //serNum4 = rfid.serNum[4];
            
                        
                //Serial.print("Hex: ");
                Serial.print(rfid.serNum[0],DEC);
                Serial.print(rfid.serNum[1],DEC);
                Serial.print(rfid.serNum[2],DEC);
                Serial.print(rfid.serNum[3],DEC);
                //Serial.print(rfid.serNum[4],DEC);
                Serial.println();
                
                digitalWrite(GREENLED, HIGH);
                digitalWrite(BUZZER, HIGH);
                delay(500);
                digitalWrite(GREENLED, LOW);
                digitalWrite(BUZZER, LOW);

            } else {
                //do nothing
               /* If we have the same ID, just write a dot. */
               //Serial.print(".");
             }
          }
    }
    
    rfid.halt();
}

