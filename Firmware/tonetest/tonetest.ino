#include "pitches.h"

// notes in the melody:
int melody[] = {
NOTE_B5,
NOTE_G4,
NOTE_F3,
NOTE_B5,
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4,8,8,4
};
void setup() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 4; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(9);
  }
}

void loop() {
  // no need to repeat the melody.
}

/*
  NOTE_B0,
  NOTE_C1,
  NOTE_D1,
  NOTE_E1,
  NOTE_F1,
  NOTE_G1,
  NOTE_A1,
  NOTE_B1,
  NOTE_C2,
  NOTE_D2,
  NOTE_E2,
  NOTE_F2,
  NOTE_G2,
  NOTE_A2,
  NOTE_B2,
  NOTE_C3,
  NOTE_D3,
  NOTE_F3,
  NOTE_G3,
  NOTE_A3,
  NOTE_B3*/