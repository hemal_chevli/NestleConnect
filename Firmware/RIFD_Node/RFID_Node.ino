//1.6.6

//add timeout for readout
// leds and buzzer
//Greeting on startup from buzzer
//smooth output use strings 
#include <MFRC522.h>
#include <RFM69.h>
#include <SPI.h>
#include "pitches.h"
// CONFIGURATION PARAMETERS
//
#define NODEID 2          // unique node ID within the closed network
#define GATEWAYID 1         // node ID of the Gateway is always 1
#define NETWORKID 100          // network ID of the network
//#define ENCRYPTKEY "xxxxxxxxxxxxxxxx"       // 16-char encryption key; same as on Gateway!
#define ENCRYPTKEY "GoldenEyesSensor"       // 16-char encryption key; same as on Gateway!
#define DEBUG           // uncomment for debugging
#define VERSION "RFID V2.0"       // this value can be queried as device 3

// Wireless settings  Match frequency to the hardware version of the radio

#define FREQUENCY RF69_433MHZ
//#define FREQUENCY RF69_868MHZ
//#define FREQUENCY RF69_915MHZ

#define IS_RFM69HW          // uncomment only for RFM69HW! 
#define ACK_TIME 50           // max # of ms to wait for an ack

// pin setting
#define RFID_RST  5       // reset pin RC522
#define RFID_SS   6        // Slave Select pin RC522
#define RFM_SS    10       // Slave Select pin RFM69

#define REDLED    7       // Red LED pin
#define GREENLED 8
#define BUZZER 9

#define SERIAL_BAUD 115200
#define HOLDOFF 500        // blocking period between RFID detection

bool  promiscuousMode = false;      // only listen to nodes within the closed network
bool  wakeUp = true;          // wakeup flag
long  lastCardDetect=-1;        // timestamp of last card detection
bool  cardDetect = false;       // flag to indicate an RFID card has been detected
bool  block=false;          // block RFID reader during holdoff
bool  redLedState=false;        // state of red LED
unsigned char UID[5] ;        // holds RFID code in numbers
String RFID_Code;           // holds RFID code in ASCII
String RFID_Code1;
String RFID_Code2;

typedef struct {          // Radio packet format
int nodeID;           // node identifier
int devID;            // device identifier 
int cmd;            // read or write
long  intVal;           // integer payload
float fltVal;           // floating payload
char  payLoad[32];          // string payload
} Message;

Message mes;

bool gotTwoCards = false;
int count = 0;

MFRC522 mfrc522(RFID_SS, RFID_RST); // Create MFRC522 instance

RFM69 radio;            // Create RFM69 instance

int melody[] = {NOTE_B5,NOTE_G4,NOTE_F3,NOTE_B5,NOTE_G4};
int noteDurations[] = {4,8,8,4,8};
int sadNoteDurations[] = {4,4,4,4,4};
//
//=====================   SETUP ========================================
//
void setup() {
  for (int thisNote = 0; thisNote < 5; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(9);
  }
#ifdef DEBUG
  Serial.begin(SERIAL_BAUD);
#endif
pinMode(REDLED, OUTPUT);              // set LED output
pinMode(GREENLED, OUTPUT);              // set LED output
pinMode(BUZZER, OUTPUT);              // set LED output
digitalWrite(REDLED, LOW);
digitalWrite(GREENLED, LOW);
digitalWrite(BUZZER, LOW);
radio.setCS(RFM_SS);                // set SS pin
radio.initialize(FREQUENCY,NODEID,NETWORKID);   // initialise radio 
#ifdef IS_RFM69HW
radio.setHighPower();           // only for RFM69HW!
#endif
radio.encrypt(ENCRYPTKEY);        // set radio encryption 
radio.promiscuous(promiscuousMode);     // only listen to closed network
wakeUp = false;            // send wakeup message
SPI.begin();
mfrc522.PCD_Init(); 

#ifdef DEBUG
  Serial.print("Node Software Version ");
  Serial.println(VERSION);
  Serial.print("\nTransmitting at ");
  Serial.print(FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(" Mhz...");
#endif

  digitalWrite(GREENLED, HIGH);
  delay(500);
  digitalWrite(GREENLED, LOW);
} // end setup

//
//
//====================    MAIN  ========================================
//
void loop() {
  block = (millis()-lastCardDetect) < HOLDOFF;
  cli();
  if (!block) {
  if (redLedState) {          // turn off red LED
      redLedState = false;
      //digitalWrite(REDLED,LOW);
      digitalWrite(BUZZER, LOW);
    }
  if ( mfrc522.PICC_IsNewCardPresent()) { // Look for new cards
      lastCardDetect = millis();
      redLedState = true;
      //digitalWrite(REDLED,HIGH);  
      //digitalWrite(BUZZER, HIGH);
    }
  if ( mfrc522.PICC_ReadCardSerial()) { // Select one of the cards
      if(UID[0]!= mfrc522.uid.uidByte[0] ||
         UID[1]!= mfrc522.uid.uidByte[1] ||
         UID[2]!= mfrc522.uid.uidByte[2] ||
         UID[3]!= mfrc522.uid.uidByte[3]){
          digitalWrite(REDLED,HIGH);  
          digitalWrite(BUZZER, HIGH);
          cardDetect = true;
          for (int i = 0; i < 4; i++) { //
            UID[i] = mfrc522.uid.uidByte[i];
            Serial.print(UID[i], DEC);
            Serial.print(" "); 
          }
          for ( int j = 0; j < 4; j++){     // fill string with 4 Hex numbers
            RFID_Code += String(UID[j],DEC);
          }
          count++;
          if(count ==1){
            RFID_Code += ",";
            digitalWrite(REDLED,HIGH);
          }
          if(count == 2){
            gotTwoCards = true;
            digitalWrite(REDLED,LOW);
          }
          
        }
  mfrc522.PICC_HaltA();         // Stop reading
  delay(100);
  digitalWrite(BUZZER, LOW);
    }
  }
  sei();
//start timer after getting one card, use count
//if count is one
  //start timer
    //if time out reset variables, rfid string and count
//send pair
  if(gotTwoCards){
    sendMsg(); 
    gotTwoCards = false;
    count = 0;
  }

}   // end loop

//=====FUNCTIONS 

void sendMsg() {          // prepares values to be transmitted
  bool tx = false;          // transmission flag
  mes.nodeID=NODEID;
  mes.intVal = 0;
  mes.fltVal = 0;
  mes.cmd = 0;            // '0' means no action needed in gateway
  int i;
  // for ( i = 0; i < sizeof(VERSION); i++){
  // mes.payLoad[i] = VERSION[i];  }
  // mes.payLoad[i] = '\0';          // software version in payload string

  if (cardDetect) {           // RFID card detected
    mes.devID = NODEID; //Change in gateway so the string is displayed
    //RFID_Code="";
    // for ( i = 0; i < 4; i++){     // fill string with 4 Hex numbers
    //   //RFID_Code += String(UID[i],HEX);
    //   RFID_Code += String(UID[i],DEC);
    // }
    for (i= 0; i<32 ; i++){   
      mes.payLoad[i] = RFID_Code[i];  
    }
    Serial.println(RFID_Code);
    cardDetect = false;
    txRadio();
    RFID_Code="";

  }
}

void txRadio()            // Transmits the 'mes'-struct to the gateway
{
cli();
if (radio.sendWithRetry(GATEWAYID, (const void*)(&mes), sizeof(mes)))
#ifdef DEBUG
  {
    Serial.print(" message ");
    Serial.print(mes.devID);
    Serial.println(" sent...");
    digitalWrite(GREENLED, HIGH);
    delay(500);
    digitalWrite(GREENLED, LOW);

  }
  else {
    Serial.println("No connection...");
    digitalWrite(BUZZER, LOW);
    delay(100);
    for (int thisNote = 0; thisNote < 2; thisNote++) {
    int noteDuration = 1000 / sadNoteDurations[thisNote];
    tone(9, melody[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(9);
  }
  }
#endif
;
sei();}  // end txRadio