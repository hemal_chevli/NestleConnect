//RFID Game


#include <SoftwareSerial.h>

#include <RFM69.h>    
#include <SPI.h>
#include <SPIFlash.h> 

#define NODEID        2    //unique for each node on same network
#define NETWORKID     100  //the same on all nodes that talk to each other
#define GATEWAYID     1
//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY   RF69_433MHZ
//#define FREQUENCY   RF69_868MHZ
//#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
//#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
#define ACK_TIME      30 // max # of ms to wait for an ack
#ifdef __AVR_ATmega1284P__
#define LED           15 // Moteino MEGAs have LEDs on D15
#define FLASH_SS      23 // and FLASH SS on D23
#else
#define LED           9 // Moteinos have LEDs on D9
#define FLASH_SS      8 // and FLASH SS on D8
#endif

#define SERIAL_BAUD   115200

//no ack
boolean requestACK = false;
RFM69 radio;

//RFID only RX is used ie D2
SoftwareSerial rfid(A1,A0); //Create a 'fake' serial port. D2 is the Rx pin, D3 is the Tx pin.
/////////////////////////
int data1 = 0;
int ok=-1;
// define the tag numbers that can have access
int tag1[12] = {48,50,48,48,53,67,67,66,67,56,53,68}; 
int tag2[12] = {48,50,48,48,53,67,67,68,68,65,52,57}; 
int tag3[12] = {48,50,48,48,53,67,67,67,67,70,53,68}; 
int tag4[12] = {48,50,48,48,53,67,67,66,67,69,53,66};
int tag5[12] = {48,50,48,48,53,67,70,53,54,52,67,70};
int tag6[12] = {48,50,48,48,53,67,67,69,69,56,55,56};
int tag7[12] = {48,50,48,48,53,67,70,52,50,50,56,56};
int tag8[12] = {48,50,48,48,53,67,67,51,69,50,55,70};
int tag9[12] = {48,50,48,48,53,68,67,66,65,67,51,56};
int tag10[12]= {48,50,48,48,53,67,70,65,65,70,48,66};
int tag11[12]= {48,50,48,48,53,67,70,57,53,69,70,57};
int tag12[12]= {48,50,48,48,53,68,52,51,68,52,67,56};
int tag13[12]= {48,50,48,48,53,68,52,50,53,66,52,54};
int tag14[12]= {48,50,48,48,53,67,67,52,68,50,52,56};
int tag15[12]= {48,50,48,48,53,67,70,50,69,54,52,65};

int newtag[12] = {0,0,0,0,0,0,0,0,0,0,0,0}; // used for read comparisons

byte tagfound=0;

int z=0;

char a;//from processing
char sendBuf[32];
byte sendLen;
void setup(){

  Serial.begin(SERIAL_BAUD);
  Serial.flush();
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower(); //uncomment only for RFM69HW!
#endif
  radio.encrypt(ENCRYPTKEY);
  //radio.setFrequency(919000000); //set frequency to some custom frequency
  char buff[50];
  sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);

 
  rfid.begin(9600);
  rfid.flush();
  pinMode(9,OUTPUT);
  digitalWrite(9,HIGH);
  Serial.print("RFID reader ACTIVE");
}

void loop(){

  readTag();

}

/********FUNCTION PROTOTYPES*******************/
boolean comparetag(int aa[12], int bb[12])
//  compares two arrrays, returns true if identical - good for comparing tags
{
  boolean ff=false;
  int fg=0;
  for (int cc=0; cc<12; cc++)
  {
    if (aa[cc]==bb[cc])
    {
      fg++;
    }
  }
  if (fg==12)
  {
    ff=true;
  }
  return ff;
}
void checkmytags()
//compares each tag against the tag just read
{
  ok=0; // this variable helps decision making, if it is 1, we have a match, zero - a read but no match, -1, no read attempt made
  if (comparetag(newtag,tag1)==true)
    //send via radio here
    tx(1);
    Serial.print("1,");
  if (comparetag(newtag,tag2)==true)
    Serial.print("2,");
  if (comparetag(newtag,tag3)==true)
    Serial.print("3,");
  if (comparetag(newtag,tag4)==true)
    Serial.print("4,");
  if (comparetag(newtag,tag5)==true)
    Serial.print("5,");
  if (comparetag(newtag,tag6)==true)
    Serial.print("6,");      
  if (comparetag(newtag,tag7)==true)
    Serial.print("7,");      
  if (comparetag(newtag,tag8)==true)
    Serial.print("8,");
  if (comparetag(newtag,tag9)==true)
    Serial.print("9,");
  if (comparetag(newtag,tag10)==true)
    Serial.print("10,");
  if (comparetag(newtag,tag11)==true)
    Serial.print("11,");
  if (comparetag(newtag,tag12)==true)
    Serial.print("12,");
  if (comparetag(newtag,tag13)==true)
    Serial.print("13,");
  if (comparetag(newtag,tag14)==true)
    Serial.print("14,");
  if (comparetag(newtag,tag15)==true)
    Serial.print("15,");


  blinkLed();

}
void blinkLed(){
  digitalWrite(9,LOW);
  delay(300);
  digitalWrite(9,HIGH);
}
void readTag() 
// poll serial port to see if tag data is coming in (i.e. a read attempt)
{
  ok=-1;
  if (rfid.available() > 0) // if a read has been attempted
  {
    // read the incoming number on serial RX
    delay(100);  // Needed to allow time for the data to come in from the serial buffer. 
    for (int z=0; z<12; z++) // read the rest of the tag
    {
      data1=rfid.read();
      // Serial.print(data1); //see raw rfid
      newtag[z]=data1;
    }
    rfid.flush(); // stops multiple reads
    // now to match tags up
    checkmytags(); // compare the number of the tag just read against my own tags' number
  }

}

void tx(int i){
   sprintf(sendBuf, "%d", i);
    sendLen = strlen(sendBuf);

    if (radio.sendWithRetry(GATEWAYID, sendBuf, sendLen))
    {
      Serial.println("OK");


    }
    else       
    Serial.println("NOK");
  
}


